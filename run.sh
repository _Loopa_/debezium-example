#!/bin/bash
docker-compose down --remove-orphans
docker build -t python_kafka_producer_my:latest kafka_producer
docker build -t python_kafka_consumer_my:latest kafka_consumer
docker build -t kafka_connect_my:latest kafka-connect
docker-compose up -d postgres-source
docker-compose up -d postgres-target
docker-compose up -d zookeeper
docker-compose up -d kafka
docker-compose up -d schema-registry
docker-compose up -d kafka-connect
docker-compose up -d kafka-ui
docker-compose up -d python-kafka-producer

until (docker logs ved_debezium_postgres_source 2>&1 | grep -q 'database system is ready to accept connections'); do sleep 1; done
echo 'SOURCE DATABASE READY!'

until (docker logs ved_debezium_postgres_target 2>&1 | grep -q 'database system is ready to accept connections'); do sleep 1; done
echo 'TARGET DATABASE READY!'

echo 'producer logs check...'
docker logs ved_debezium_producer
echo 'zookeeper logs check...'
docker logs ved_debezium_zookeeper
echo 'schema-registry logs check...'
docker logs ved_debezium_schema_registry

echo 'CONNECTING TO KAFKA...'

until (docker logs ved_debezium_kafka_connect 2>&1 | grep -q 'Finished starting connectors and tasks '); do sleep 1; done
docker exec ved_debezium_kafka_connect curl -H 'Content-Type: application/json' kafka-connect:8083/connectors -d "$(envsubst < debezium-config.json)"
docker exec ved_debezium_kafka_connect curl -H 'Content-Type: application/json' kafka-connect:8083/connectors -d "$(envsubst < jdbc-sink-connector.json)"

docker-compose up -d python-kafka-consumer

sleep 120

docker logs ved_debezium_consumer

sleep 30

docker-compose down
