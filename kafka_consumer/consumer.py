# from kafka import KafkaConsumer, KafkaAdminClient
from confluent_kafka import Consumer, TopicPartition, KafkaException
from confluent_kafka.admin import AdminClient, NewPartitions, ConfigResource, ResourceType, ConfigSource
import json
import logging
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from time import sleep
from datetime import date

# ______________________ DATABASE_____________________

Base = declarative_base()


class Item(Base):
    __tablename__ = 'shipments'
    shipment_id = Column(Integer, primary_key=True)
    order_id = Column(Integer)
    date_created = Column(Date())
    status = Column(String(20))

    def __init__(self, shipment_id, order_id, date_created, status):
        self.shipment_id = shipment_id
        self.order_id = order_id
        self.date_created = date_created
        self.status = status

    def __repr__(self):
        return "<ApiUser('%s','%s', '%s', '%s')>" % (self.shipment_id, self.order_id, self.date_created, self.status)


def read_last_db_record(session):
    found_order = session.query(Item).order_by(desc(Item.shipment_id)).first()
    d = dict([
        ('Shipment ID', found_order.shipment_id),
        ('Order ID', found_order.order_id),
        ('Date created', found_order.date_created),
        ('Status', found_order.status)
    ])
    return d


engine = create_engine(
    'postgresql://postgresuser:postgrespw@ved_debezium_postgres_source:5432/shipment_source_db'
)
Session_search = sessionmaker(bind=engine)
s = Session_search()

# __________________________KAFKA______________________________

admin_kafka = AdminClient({'bootstrap.servers': 'ved_debezium_kafka:9092'})
partition_create = admin_kafka.create_partitions(
    [NewPartitions('shipments_db.public.shipments', 1)],
    validate_only=False
)
print('partition created for topic ', partition_create.values())

consumer = Consumer({'bootstrap.servers': 'ved_debezium_kafka:9092',
                     'auto.offset.reset': 'latest',
                     'group.id': 'myConsumer'
                     })

consumer.subscribe(['shipments_db.public.shipments'])
consumer.assign([TopicPartition('shipments_db.public.shipments', 1)])


# _____________________________________LOGGER__________________________


def convert_config_to_str(config, depth):
    return ('\n\t\t%40s = %-50s  [%s,is:read-only=%r,default=%r,sensitive=%r,synonym=%r,synonyms=%s]' %
            ((' ' * depth) + config.name, config.value, ConfigSource(config.source),
             config.is_read_only, config.is_default,
             config.is_sensitive, config.is_synonym,
             ["%s:%s" % (x.name, ConfigSource(x.source))
              for x in iter(config.synonyms.values())]))


def read_config(admin_client: AdminClient, sourse_type_num, source_id):
    future_set = admin_client.describe_configs([ConfigResource(ResourceType(sourse_type_num), source_id)])
    for res, future in future_set.items():
        try:
            configs = future.result()
            for config in iter(configs.values()):
                return convert_config_to_str(config, 1)

        except KafkaException as e:
            print("Failed to describe {}: {}".format(res, e))
        except Exception:
            raise


logging.basicConfig(
    level=logging.INFO,
    filename='logs/consumer.log',
    format='%(asctime)s %(levelname)s:%(message)s'
)
logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)
logger = logging.getLogger("consumer")

logger.info(
    "\n\t\t\t\tINIT CONSUMER CONFIGURATION:\nTopic info:\n\t%s\nTopic Partition info:\n\t%s\n",
    consumer.list_topics(),
    consumer.assignment()
)
logger.info(
    "\n\t\t\t\tINIT ADMIN CONFIGURATION:\nTopics info:\n\t%s\nConsumers groups info:\n\t%s\nBroker "
    "config:\n\t%s\nPython consumer config:\n\t%s\nDB consumer config:\n\t%s\n",
    admin_kafka.list_topics(),
    admin_kafka.list_groups(),
    read_config(admin_kafka, 2, 'shipments_db.public.shipments'),
    read_config(admin_kafka, 3, 'myConsumer'),
    read_config(admin_kafka, 3, 'connect-shipments_target_db_jdbc_sink_connector')
)
logger.info("Handling messages")

print('start logging!')

while True:
    try:
        message = consumer.poll(1.0)
        print('new message from kafka!')
        if message is None:
            continue
        if message.error():
            logger.error("Problem with handling message from Kafka")
            continue

        print('Received message: {}'.format(message.value().decode('utf-8')))

        topic = message.topic()
        partition = message.partition()
        offset = message.offset()
        key = json.loads(message.key().decode('utf-8'))['payload']
        value = json.loads(message.value().decode('utf-8'))['payload']['after']

        logger.info(
            "New message in topic %s, partition %d, offset %d, message key %s:\n\t%s\n",
            topic,
            partition,
            offset,
            key,
            value
        )

        try:
            print("compare with target DB record")
            db_record = read_last_db_record(s)
            logger.info("\n\tRecord of new message in target DB:\n%s", db_record)
        except:
            print("DB error!")
            logger.error("Problem with reading record from database for message:\n%s", value)
        logger.debug("The %s:%d:%d message handled!", topic, partition, offset)
    except:
        logger.error("Problem with Kafka polling")

consumer.close()
