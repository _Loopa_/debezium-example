from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from time import sleep
from datetime import date


Base = declarative_base()


class Item(Base):
    __tablename__ = 'shipments'
    shipment_id = Column(Integer, primary_key=True)
    order_id = Column(Integer)
    date_created = Column(Date())
    status = Column(String(20))

    def __init__(self, shipment_id, order_id, date_created, status):
        self.shipment_id = shipment_id
        self.order_id = order_id
        self.date_created = date_created
        self.status = status

    def __repr__(self):
        return "<ApiUser('%s','%s', '%s', '%s')>" % (self.shipment_id, self.order_id, self.date_created, self.status)


def input_item(new_shipment_id, new_order_id, new_date_created, new_status, session):
    new_user = Item(new_shipment_id, new_order_id, new_date_created, new_status)
    session.add(new_user)
    session.commit()


engine = create_engine('postgresql://postgresuser:postgrespw@ved_debezium_postgres_source:5432/shipment_source_db', echo=True)

Base.metadata.create_all(engine)
metadata = Base.metadata
users_table = Item.__table__

Session_search = sessionmaker(bind=engine)
s = Session_search()

init_shipment_id = 35500
init_order_id = 15500
init_year = 2001
init_date_created = date(2001, 12, 9)
init_status = "PROCESSING"

while True:
    # create item
    input_item(init_shipment_id, init_order_id, init_date_created, init_status, s)
    sleep(60)

    init_shipment_id += 10
    init_order_id += 10
    init_year += 1
    init_date_created = date(init_year, 12, 9)
